^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
Changelog for package oros_tools_examples
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

0.1.3 (2017-05-05)
------------------
* switch to stage simulator & add dependencies
* Contributors: Gérald Lelong

0.1.1 (2017-04-24)
------------------
* ready to release
* modified launch/turtlebot.launch
* first
* Contributors: Gérald Lelong, nmartignoni
